<?php

namespace App\Controller;

use App\Models\Deck;
use App\Models\Game;
use App\Form\Type\GameType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class GameController extends AbstractController
{
    private const HEAD_TITLE = "CardGame";
    private const PAGE_TITLE = "Simple Card Game";

    /**
     * @Route("/", name="configuration")
     */
    public function configuration(Request $request): Response
    {
        $game = new Game();

        $form = $this->createForm(GameType::class, $game);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $game = $form->getData();

            return $this->redirectToRoute('startGame', ['cardNumber' => $game->getPlayerDeckSize()]);
        }

        return $this->render('game/configuration.html.twig', [
            'head_title' => $this::HEAD_TITLE,
            'page_title' => $this::PAGE_TITLE,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/game/{cardNumber}", name="startGame", requirements={"cardNumber"="\d+", })
     */
    public function startGame(int $cardNumber): Response
    {
        $game = new Game($cardNumber);
        $deck = new Deck();
        $game->setGameDeck($deck);
        $game->initPlayerDeck();

        return $this->render('game/game.html.twig', [
            'head_title' => $this::HEAD_TITLE,
            'page_title' => $this::PAGE_TITLE,
            'game' => $game,
            'sortedPlayerDeck' => $game->getPlayerDeck()->sortPerOrder($game->getOrderedSuits(), $game->getOrderedValues()),
        ]);
    }
}
