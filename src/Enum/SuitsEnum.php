<?php

namespace App\Enum;

class SuitsEnum
{
    const SUITS = [
        '♥',
        '♠',
        '♦',
        '♣',
    ];
}