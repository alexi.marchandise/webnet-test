<?php

namespace App\Enum;

class ValuesEnum
 {
     const VALUES = [
        "1",
        "2",
        "3",
        "4",
        "5",
        "6",
        "7",
        "8",
        "9",
        "10",
        "V",
        "D",
        "R",
    ];
 }