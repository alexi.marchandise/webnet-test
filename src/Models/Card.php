<?php

namespace App\Models;

class Card 
{
    private $suit = "";
    private $value = "";

    function __construct(string $suit, string $value)
    {
        $this->suit = $suit;
        $this->value = $value;
    }

    public function getSuit(): string
    {
        return $this->suit;
    }

    public function setSuit(string $suit): void
    {
        $this->suit = $suit;
    }
    public function getValue(): string
    {
        return $this->value;
    }

    public function setValue(string $value): void
    {
        $this->value = $value;
    }
}