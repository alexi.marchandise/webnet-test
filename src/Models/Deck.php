<?php

namespace App\Models;

use App\Enum\SuitsEnum;
use App\Enum\ValuesEnum;

class Deck
{
    private $suits = SuitsEnum::SUITS;
    private $values = ValuesEnum::VALUES;
    private $cards= [];

    function __construct($cards = []) 
    {
        if($cards !== [])
        {
            $this->setCards($cards);
            return;
        }

        $this->createDeck();
    }

    public function setCards(array $cards): void
    {
        $this->cards = $cards;
    }

    public function getCards(): array
    {
        return $this->cards;
    }

    public function getSuits(): array
    {
        return $this->suits;
    }

    public function getValues(): array
    {
        return $this->values;
    }

    public function shuffle(): bool
    {
        return shuffle($this->cards);
    }

    private function createDeck()
    {
        foreach ($this->suits as $suit) 
        {
            foreach($this->values as $value) {
                array_push($this->cards, new Card($suit, $value));
            }
        }

        $this->shuffle();
    }

    public function pickCard(int $cardPosition): Card
    {
        $card = $this->cards[$cardPosition];
        array_splice($this->cards, $cardPosition, 1);

        return $card;

    }

    public function sortPerOrder(array $orderedSuits, array $orderedValues): Deck
    {
        $cardsPerSuits = $this->sortPerSuit($this->cards, $orderedSuits);
        $sortedCards = $this->sortPerValues($cardsPerSuits, $orderedValues);

        $sortedCards = array_merge(...array_values($sortedCards));

        return new Deck($sortedCards);
    }

    private function sortPerSuit(array $cards, array $orderedSuits): array
    {
        $cardsPerSuits = [];

        foreach ($orderedSuits as $suit) {
            $cardsPerSuits[$suit] = [];

            foreach ($this->cards as $card)
            {
                if ($card->getSuit() === $suit)
                {
                    $cardsPerSuits[$suit][] = $card;
                }
            }
        }

        return $cardsPerSuits;
    }

    private function sortPerValues(array $cardsPerSuit, array $orderedValues): array
    {
        $sortedCards = [];
        $orderedSuits = array_keys($cardsPerSuit);

        foreach ($orderedSuits as $suit)
        {
            foreach ($orderedValues as $value)
            {
                foreach ($cardsPerSuit[$suit] as $card)
                {
                    if ($card->getValue() === $value)
                    {
                        $sortedCards[$suit][] = $card;
                    }
                }
            }
        }

        return $sortedCards;
    }
}
