<?php

namespace App\Models;

use App\Enum\SuitsEnum;
use App\Enum\ValuesEnum;

class Game 
{
    protected int $playerDeckSize;
    private Deck $playerDeck;
    private Deck $gameDeck;
    private array $orderedSuits = SuitsEnum::SUITS;
    private array $orderedValues = ValuesEnum::VALUES;


    function __construct($playerDeckSize = 7)
    {
        $this->playerDeckSize = $playerDeckSize;
        shuffle($this->orderedSuits);
        shuffle($this->orderedValues);
    }

    public function getPlayerDeckSize(): ?int
    {
        return $this->playerDeckSize;
    }

    public function setPlayerDeckSize(int $playerDeckSize): void
    {
        $this->playerDeckSize = $playerDeckSize;
    }

    public function setGameDeck(Deck $gameDeck)
    {
        $this->gameDeck = $gameDeck;
    }

    public function getGameDeck(): Deck
    {
        return $this->gameDeck;
    }

    public function initPlayerDeck(): void
    {
        $playerDeckCards = [];

        for ($i = 0; $i < $this->playerDeckSize; $i++)
        {
            $cardPosition = random_int(0, count($this->gameDeck->getCards())-1);
            $playerDeckCards[] = $this->gameDeck->pickCard($cardPosition);

            $this->playerDeck = new Deck($playerDeckCards);
        }
    }

    public function setPlayerDeck(Deck $playerDeck)
    {
        $this->playerDeck = $playerDeck;
    }

    public function getPlayerDeck(): Deck
    {
        return $this->playerDeck;
    }

    public function getOrderedSuits(): array
    {
        return $this->orderedSuits;
    }

    public function getOrderedValues(): array
    {
        return $this->orderedValues;
    }
}